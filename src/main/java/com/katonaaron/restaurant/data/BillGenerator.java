package com.katonaaron.restaurant.data;

import com.katonaaron.restaurant.business.Order;
import com.katonaaron.restaurant.business.menu.MenuItem;

import java.io.Serializable;
import java.util.Collection;

public interface BillGenerator extends Serializable {
    void generateBill(Order order, Collection<MenuItem> menuItems, double total);
}
