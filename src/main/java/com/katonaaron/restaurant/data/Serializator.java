package com.katonaaron.restaurant.data;

import com.katonaaron.restaurant.exception.SerializationException;

import java.io.*;

public class Serializator<T extends Serializable> {

    private final String fileName;

    public Serializator(String fileName) {
        this.fileName = fileName;
    }

    public void serialize(T t) {
        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(fileName));
            outputStream.writeObject(t);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            throw new SerializationException(e);
        }
    }

    @SuppressWarnings("unchecked")
    public T deserialize() {
        T t;
        try {
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(fileName));
            t = (T) inputStream.readObject();
            inputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            throw new SerializationException(e);
        }
        return t;
    }
}
