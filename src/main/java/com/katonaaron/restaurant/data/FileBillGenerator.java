package com.katonaaron.restaurant.data;

import com.katonaaron.restaurant.business.Order;
import com.katonaaron.restaurant.business.menu.MenuItem;
import com.katonaaron.restaurant.exception.BillGenerationException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;

public class FileBillGenerator implements BillGenerator {

    private static final long serialVersionUID = -8363701719063093049L;

    public void generateBill(Order order, Collection<MenuItem> menuItems, double total) {
        try (FileWriter fileWriter = new FileWriter("bill-" + order.getId() + ".txt")) {
            fileWriter.append("Order: ").append(String.valueOf(order.getId()))
                    .append("\nTable: ").append(String.valueOf(order.getTable()))
                    .append("\nDate: ").append(order.getDate().toString())
                    .append("\nMenu items:\n");
            int i = 0;
            for (MenuItem item : menuItems) {
                fileWriter.append(String.valueOf(++i))
                        .append(". ")
                        .append(item.getName())
                        .append("\t")
                        .append(String.valueOf(item.computePrice()))
                        .append(" RON\n");
            }
            fileWriter.append("Total: ").append(String.valueOf(total)).append(" RON");
            fileWriter.flush();
        } catch (IOException e) {
            throw new BillGenerationException(e);
        }
    }
}
