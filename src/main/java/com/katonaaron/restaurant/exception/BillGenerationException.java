package com.katonaaron.restaurant.exception;

public class BillGenerationException extends RuntimeException {
    public BillGenerationException() {
    }

    public BillGenerationException(String message) {
        super(message);
    }

    public BillGenerationException(String message, Throwable cause) {
        super(message, cause);
    }

    public BillGenerationException(Throwable cause) {
        super(cause);
    }
}
