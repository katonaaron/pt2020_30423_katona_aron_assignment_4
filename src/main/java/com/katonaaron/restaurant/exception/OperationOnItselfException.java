package com.katonaaron.restaurant.exception;

public class OperationOnItselfException extends RuntimeException {
    public OperationOnItselfException() {
    }

    public OperationOnItselfException(String message) {
        super(message);
    }

    public OperationOnItselfException(String message, Throwable cause) {
        super(message, cause);
    }

    public OperationOnItselfException(Throwable cause) {
        super(cause);
    }
}
