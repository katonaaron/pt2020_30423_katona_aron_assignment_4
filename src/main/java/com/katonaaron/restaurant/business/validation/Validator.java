package com.katonaaron.restaurant.business.validation;

import com.katonaaron.restaurant.exception.ValidationException;

import java.io.Serializable;
import java.util.Objects;

public interface Validator<T> extends Serializable {
    void validate(T t) throws ValidationException;


    default Validator<T> andThen(Validator<T> after) {
        Objects.requireNonNull(after);
        return (T t) -> {
            validate(t);
            after.validate(t);
        };
    }
}
