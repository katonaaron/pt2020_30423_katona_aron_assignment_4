package com.katonaaron.restaurant.business.validation;

import com.katonaaron.restaurant.business.menu.MenuItem;
import com.katonaaron.restaurant.exception.ValidationException;


public class MenuItemNameValidator implements Validator<MenuItem> {
    private static final long serialVersionUID = -3962959241323099339L;

    @Override
    public void validate(MenuItem menuItem) throws ValidationException {
        if (menuItem.getName() == null) {
            throw new ValidationException("Name of menu item is null");
        }
        if (menuItem.getName().isEmpty()) {
            throw new ValidationException("Name of the menu item is empty");
        }
    }
}
