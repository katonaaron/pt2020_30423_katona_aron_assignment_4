package com.katonaaron.restaurant.business.validation;

import com.katonaaron.restaurant.business.menu.MenuItem;
import com.katonaaron.restaurant.exception.ValidationException;

public class MenuItemPriceValidator implements Validator<MenuItem> {
    private static final long serialVersionUID = 3296242932826092785L;

    @Override
    public void validate(MenuItem menuItem) throws ValidationException {
        if (menuItem.computePrice() < 0) {
            throw new ValidationException("Price must be >= 0");
        }
    }
}
