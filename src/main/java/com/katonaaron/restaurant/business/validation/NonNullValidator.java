package com.katonaaron.restaurant.business.validation;

import com.katonaaron.restaurant.exception.ValidationException;

public class NonNullValidator<T> implements Validator<T> {
    private static final long serialVersionUID = 7790632250693336592L;

    @Override
    public void validate(T t) throws ValidationException {
        if (t == null) {
            throw new IllegalArgumentException("Argument is null");
        }
    }
}
