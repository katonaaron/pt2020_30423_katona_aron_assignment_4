package com.katonaaron.restaurant.business;

import com.katonaaron.restaurant.business.menu.MenuItem;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

/**
 * Interface for representing a restaurant. Allows the management of menu items, orders and provides bill generation.
 */
public interface IRestaurantProcessing extends Serializable {
    /**
     * Adds the menu item to the list menu items of the restaurant.
     *
     * @param menuItem menu item to be added.
     * @pre menuItem != null
     * @pre menuItem.getName() != null &amp;&amp; !menuItem.getName().isEmpty()
     * @pre menuItem.computePrice() &ge; 0
     * @pre !containsMenuItem(menuItem)
     * @post getAllMenuItems().size() == getAllMenuItems().size()@pre + 1
     * @post containsMenuItem(menuItem)
     */
    void createMenuItem(MenuItem menuItem);

    /**
     * Deletes a menu item.
     *
     * @param menuItem menu item to be deleted.
     * @pre menuItem != null
     * @pre menuItem.getName() != null
     * @pre containsMenuItem(menuItem)
     * @post getAllMenuItems().size() == getAllMenuItems().size()@pre - 1
     * @post !containsMenuItem(menuItem)
     */
    void deleteMenuItem(MenuItem menuItem);

    /**
     * Deletes a menu item which has the given name.
     *
     * @param name name of the menu item to be deleted.
     * @pre name != null
     * @pre containsMenuItem(name)
     * @post getAllMenuItems().size() == getAllMenuItems().size()@pre - 1
     * @post !containsMenuItem(name)
     */
    void deleteMenuItemByName(String name);

    /**
     * The existing menu item having the given name will be updated with the given menu item.
     *
     * @param itemName name of the menu item to be updated.
     * @param newItem  data which will be used for updating the existing menu item
     * @pre itemName != null &amp;&amp; newItem != null
     * @pre containsMenuItem(itemName)
     * @post containsMenuItem(newItem.getName ())
     * @post getMenuItemByName(newItem.getName ()).equals(newItem)
     * @post getMenuItemByName(newItem.getName ()) == getMenuItemByName(itemName)@pre
     */
    void updateMenuItem(String itemName, MenuItem newItem);

    /**
     * The existing menu item given as a parameter is updated in the menu.
     * Its name must be the same as in the restaurant. Otherwise use {@link #updateMenuItem(String, MenuItem)}.
     *
     * @param menuItem menu item to be updated.
     * @pre menuItem != null &amp;&amp; menuItem.getName() != null
     * @pre containsMenuItem(menuItem)
     * @post containsMenuItem(menuItem.getName ())
     * @post getMenuItemByName(menuItem.getName ()).equals(menuItem)
     * @post getMenuItemByName(menuItem.getName ()) == getMenuItemByName(menuItem.getName ())@pre
     */
    void updateMenuItem(MenuItem menuItem);

    /**
     * Returns the menu item of the restaurant with the given name.
     *
     * @param name name of the menu item/
     * @return the menu item having the given name.
     * @pre name != null
     * @pre containsMenuItem(name)
     * @post @result.getName().equals(name)
     * @post @nochange
     */
    MenuItem getMenuItemByName(String name);

    /**
     * Returns all the menu items of the restaurant.
     *
     * @return list of menu items.
     * @pre true
     * @post @nochange
     */
    List<MenuItem> getAllMenuItems();

    /**
     * Verifies whether the given menu item exists in the restaurant.
     *
     * @param menuItem menu item to be verified.
     * @return true if the item exists in the restaurant.
     * @pre menuItem != null
     * @pre menuItem.getName() != null
     * @post @nochange
     */
    boolean containsMenuItem(MenuItem menuItem);

    /**
     * Verifies whether a menu item exists in the restaurant, having hte given name.
     *
     * @param name name to be verified.
     * @return true if an item exists in the restaurant with the given name.
     * @pre name != null
     * @post @nochange
     */
    boolean containsMenuItem(String name);

    /**
     * Places a new order in the restaurant. Observers are notified.
     *
     * @param order     order to be placed.
     * @param menuItems menu items of the order.
     * @pre order != null
     * @pre order.getDate != null
     * @pre menuItems != null
     * @pre items.size() &gt; 0
     * @pre @forall i : [0 .. items.size() - 1] @ containsMenuItem(items.get(i))
     * @post getAllOrders().size() == getAllOrders().size()@pre + 1
     */
    void createOrder(Order order, Collection<MenuItem> menuItems);

    /**
     * Computes the price of an existing order.
     *
     * @param order order whose price are required to be calculated.
     * @return the total sum of the items of the order.
     * @pre order != null
     * @pre containsOrder(order)
     * @post @nochange
     */
    double computeOrderPrice(Order order);

    /**
     * Generates a bill for the given order.
     *
     * @param order order for which the bill is calculated.
     * @pre order != null
     * @pre containsOrder(order)
     * @post @nochange
     */
    void generateBill(Order order);

    /**
     * Returns all orders of the restaurant.
     *
     * @return list of orders of the restaurant.
     * @pre true
     * @post @nochange
     */
    List<Order> getAllOrders();

    /**
     * Verifies whether an order exists in the restaurant.
     *
     * @param order order to be verified.
     * @return true if the order is present in the restaurant.
     * @pre order != null
     * @post @nochange
     */
    boolean containsOrder(Order order);
}
