package com.katonaaron.restaurant.business.menu;

import com.katonaaron.restaurant.exception.OperationOnItselfException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CompositeProduct extends MenuItem {
    private static final long serialVersionUID = 3582541522810798594L;

    private List<MenuItem> items;
    private Double price;

    public CompositeProduct(String name) {
        this(name, null);
    }

    public CompositeProduct(String name, Double price) {
        super(name);
        this.price = price;
        this.items = new ArrayList<>();
    }

    public void add(MenuItem item) {
        if (super.equals(item)) {
            throw new OperationOnItselfException("Cannot add item as a component of itself");
        }
        items.add(item);
    }

    public void add(List<MenuItem> items) {
        items.forEach(this::add);
    }

    public void remove(MenuItem item) {
        items.remove(item);
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public List<MenuItem> getComponents() {
        return List.copyOf(items);
    }

    @Override
    public void update(MenuItem newMenuItem) {
        if (!(newMenuItem instanceof CompositeProduct)) {
            throw new IllegalArgumentException("not a CompositeProduct");
        }
        super.update(newMenuItem);
        CompositeProduct newProduct = (CompositeProduct) newMenuItem;
        items = new ArrayList<>(newProduct.items);
        price = newProduct.price;
    }

    @Override
    public List<MenuItem> getBaseItems() {
        return items.stream()
                .map(MenuItem::getBaseItems)
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    @Override
    public double computePrice() {
        if (price != null) {
            return price;
        }
        return items.stream()
                .mapToDouble(MenuItem::computePrice)
                .sum();
    }

    @Override
    public ProductType getType() {
        return ProductType.COMPOSITE;
    }

    @Override
    public ProductPriceType getPriceType() {
        if (price == null) {
            return ProductPriceType.AUTOMATIC;
        } else {
            return ProductPriceType.EXPLICIT;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CompositeProduct)) return false;
        if (!super.equals(o)) return false;
        CompositeProduct that = (CompositeProduct) o;
        return Objects.equals(items, that.items);
    }
}
