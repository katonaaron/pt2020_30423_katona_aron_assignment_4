package com.katonaaron.restaurant.business.menu;

import java.util.ArrayList;
import java.util.List;

public class BaseProduct extends MenuItem {
    private static final long serialVersionUID = 2201337488429225484L;

    private double price;

    public BaseProduct(String name, double price) {
        super(name);
        this.price = price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public void update(MenuItem newMenuItem) {
        if (!(newMenuItem instanceof BaseProduct)) {
            throw new IllegalArgumentException("not a BaseProduct");
        }
        super.update(newMenuItem);
        setPrice(newMenuItem.computePrice());
    }

    @Override
    public List<MenuItem> getBaseItems() {
        return new ArrayList<>(List.of(this));
    }

    @Override
    public double computePrice() {
        return price;
    }

    @Override
    public ProductType getType() {
        return ProductType.BASE;
    }

    @Override
    public ProductPriceType getPriceType() {
        return ProductPriceType.EXPLICIT;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BaseProduct)) return false;
        if (!super.equals(o)) return false;
        BaseProduct that = (BaseProduct) o;
        return Double.compare(that.price, price) == 0;
    }
}
