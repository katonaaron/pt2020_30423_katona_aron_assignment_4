package com.katonaaron.restaurant.business.menu;

public enum ProductPriceType {
    EXPLICIT,
    AUTOMATIC
}
