package com.katonaaron.restaurant.business.menu;

public enum ProductType {
    BASE("Base product"),
    COMPOSITE("Composite product");

    private final String description;

    ProductType(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
