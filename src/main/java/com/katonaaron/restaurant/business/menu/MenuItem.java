package com.katonaaron.restaurant.business.menu;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public abstract class MenuItem implements Serializable {
    private static final long serialVersionUID = 5789426934723557530L;

    private String name;

    public MenuItem(String name) {
        this.name = name;
    }

    public void update(MenuItem newMenuItem) {
        this.name = newMenuItem.name;
    }

    public String getName() {
        return name;
    }

    abstract public List<MenuItem> getBaseItems();

    abstract public double computePrice();

    abstract public ProductType getType();

    abstract public ProductPriceType getPriceType();

    @Override
    public String toString() {
        return "MenuItem{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MenuItem)) return false;
        MenuItem menuItem = (MenuItem) o;
        return Objects.equals(getName(), menuItem.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }
}
