package com.katonaaron.restaurant.business;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

public class Order implements Serializable {
    private static final long serialVersionUID = 112849957316473470L;

    private final Date date;
    private final int table;
    private final int id;

    public Order(int id, Date date, int table) {
        this.id = id;
        this.date = date;
        this.table = table;
    }

    public Order(Date date, int table) {
        this.id = -1;
        this.date = date;
        this.table = table;
    }

    public int getId() {
        return id;
    }

    public Date getDate() {
        return (Date) date.clone();
    }

    public int getTable() {
        return table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return id == order.id &&
                table == order.table &&
                Objects.equals(date, order.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, table);
    }
}
