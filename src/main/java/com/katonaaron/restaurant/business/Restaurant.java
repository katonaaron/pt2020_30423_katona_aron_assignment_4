package com.katonaaron.restaurant.business;

import com.katonaaron.restaurant.business.menu.MenuItem;
import com.katonaaron.restaurant.data.BillGenerator;
import com.katonaaron.restaurant.observer.Observable;
import com.katonaaron.restaurant.observer.Observer;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a restaurant. Allows the management of menu items, orders and provides bill generation.
 *
 * @invariant isWellFormed()
 */
public class Restaurant implements IRestaurantProcessing {
    /**
     * Serialization identifier.
     */
    private static final long serialVersionUID = 6965443314766590517L;

    /**
     * Menu items of the restaurant.
     */
    private final Map<String, MenuItem> menuItems;
    /**
     * Orders of the restaurant.
     */
    private final Map<Order, Collection<MenuItem>> orders;

    /**
     * Observable used for providing subscription for new orders.
     */
    private final Observable<Collection<MenuItem>> observable;
    /**
     * Used for generating order bills.
     */
    private final BillGenerator billGenerator;

    /**
     * Id of the next incoming order.
     */
    private int nextOrderId = 1;

    /**
     * Constructs a restaurant type.
     *
     * @param billGenerator Object used for generating order bills.
     * @post isWellFormed()
     */
    public Restaurant(BillGenerator billGenerator) {
        this.billGenerator = billGenerator;
        menuItems = new HashMap<>();
        orders = new HashMap<>();
        observable = new Observable<>();
        assert isWellFormed();
    }

    /**
     * Subscribes an observer to new order.
     * The observer which will receive a copy of the menu items of the order.
     *
     * @param observer the observer which subscribes to the observable.
     * @pre observer != null
     */
    public void addObserver(Observer<Collection<MenuItem>> observer) {
        assert observer != null;
        assert isWellFormed();
        observable.addObserver(observer);
        assert isWellFormed();
    }

    /**
     * Removes an observer from the list of observers.
     *
     * @param observer the observer to be removed.
     * @pre observer != null
     */
    public void removeObserver(Observer<Collection<MenuItem>> observer) {
        assert observer != null;
        assert isWellFormed();
        observable.removeObserver(observer);
        assert isWellFormed();
    }

    /**
     * Adds the menu item to the list menu items of the restaurant.
     *
     * @param menuItem menu item to be added.
     * @pre menuItem != null
     * @pre menuItem.getName() != null &amp;&amp; !menuItem.getName().isEmpty()
     * @pre menuItem.computePrice() &ge; 0
     * @pre !containsMenuItem(menuItem)
     * @post getAllMenuItems().size() == getAllMenuItems().size()@pre + 1
     * @post containsMenuItem(menuItem)
     */
    @Override
    public void createMenuItem(MenuItem menuItem) {
        assert menuItem != null : "Menu item is null";
        assert menuItem.getName() != null && !menuItem.getName().isEmpty() : "Menu item name is null or empty";
        assert menuItem.computePrice() >= 0 : "Menu item price must be >= 0";
        assert !containsMenuItem(menuItem) : "MenuItem with name \"" + menuItem.getName() + "\" already exists";
        assert isWellFormed();
        final int sizePre = getAllMenuItems().size();
        assert isWellFormed();

        menuItems.put(menuItem.getName(), menuItem);

        assert getAllMenuItems().size() == sizePre + 1;
        assert containsMenuItem(menuItem);
        assert isWellFormed();
    }

    /**
     * Deletes a menu item.
     *
     * @param menuItem menu item to be deleted.
     * @pre menuItem != null
     * @pre menuItem.getName() != null
     * @pre containsMenuItem(menuItem)
     * @post getAllMenuItems().size() == getAllMenuItems().size()@pre - 1
     * @post !containsMenuItem(menuItem)
     */
    @Override
    public void deleteMenuItem(MenuItem menuItem) {
        assert menuItem != null : "Menu item is null";
        assert menuItem.getName() != null : "Menu item name is null";
        assert containsMenuItem(menuItem) : "MenuItem with name \"" + menuItem.getName() + "\" does not exists";
        final int sizePre = getAllMenuItems().size();
        assert isWellFormed();

        deleteMenuItemByName(menuItem.getName());

        assert getAllMenuItems().size() == sizePre - 1;
        assert !containsMenuItem(menuItem);
        assert isWellFormed();
    }

    /**
     * Deletes a menu item which has the given name.
     *
     * @param name name of the menu item to be deleted.
     * @pre name != null
     * @pre containsMenuItem(name)
     * @post getAllMenuItems().size() == getAllMenuItems().size()@pre - 1
     * @post !containsMenuItem(name)
     */
    @Override
    public void deleteMenuItemByName(String name) {
        assert name != null : "Name is null";
        assert containsMenuItem(name) : "MenuItem with name \"" + name + "\" does not exists";
        final int sizePre = getAllMenuItems().size();
        assert isWellFormed();

        menuItems.remove(name);

        assert getAllMenuItems().size() == sizePre - 1;
        assert !containsMenuItem(name);
        assert isWellFormed();
    }

    /**
     * The existing menu item having the given name will be updated with the given menu item.
     *
     * @param itemName name of the menu item to be updated.
     * @param newItem  data which will be used for updating the existing menu item
     * @pre itemName != null &amp;&amp; newItem != null
     * @pre containsMenuItem(itemName)
     * @post containsMenuItem(newItem.getName ())
     * @post getMenuItemByName(newItem.getName ()).equals(newItem)
     * @post getMenuItemByName(newItem.getName ()) == getMenuItemByName(itemName)@pre
     */
    @Override
    public void updateMenuItem(String itemName, MenuItem newItem) {
        assert itemName != null && newItem != null : "Null argument";
        assert containsMenuItem(itemName) : "MenuItem with name \"" + itemName + "\" does not exists";
        final MenuItem itemPre = getMenuItemByName(itemName);
        assert isWellFormed();

        MenuItem item = menuItems.get(itemName);
        item.update(newItem);
        if (!item.getName().equals(itemName)) {
            menuItems.remove(itemName);
            menuItems.put(item.getName(), item);
        }

        assert containsMenuItem(newItem.getName());
        assert getMenuItemByName(newItem.getName()).equals(newItem);
        assert getMenuItemByName(newItem.getName()) == itemPre;
        assert isWellFormed();
    }

    /**
     * The existing menu item given as a parameter is updated in the menu.
     * Its name must be the same as in the restaurant. Otherwise use {@link #updateMenuItem(String, MenuItem)}.
     *
     * @param menuItem menu item to be updated.
     * @pre menuItem != null &amp;&amp; menuItem.getName() != null
     * @pre containsMenuItem(menuItem)
     * @post containsMenuItem(menuItem.getName ())
     * @post getMenuItemByName(menuItem.getName ()).equals(menuItem)
     * @post getMenuItemByName(menuItem.getName ()) == getMenuItemByName(menuItem.getName ())@pre
     */
    @Override
    public void updateMenuItem(MenuItem menuItem) {
        assert menuItem != null && menuItem.getName() != null : "Null argument";
        assert containsMenuItem(menuItem) : "MenuItem with name \"" + menuItem.getName() + "\" does not exists";
        final MenuItem itemPre = getMenuItemByName(menuItem.getName());
        assert isWellFormed();

        updateMenuItem(menuItem.getName(), menuItem);

        assert containsMenuItem(menuItem.getName());
        assert getMenuItemByName(menuItem.getName()).equals(menuItem);
        assert getMenuItemByName(menuItem.getName()) == itemPre;
        assert isWellFormed();
    }

    /**
     * Returns the menu item of the restaurant with the given name.
     *
     * @param name name of the menu item/
     * @return the menu item having the given name.
     * @pre name != null
     * @pre containsMenuItem(name)
     * @post @result.getName().equals(name)
     * @post @nochange
     */
    @Override
    public MenuItem getMenuItemByName(String name) {
        assert name != null : "Name is null";
        assert containsMenuItem(name) : "MenuItem with name \"" + name + "\" does not exists";
        assert isWellFormed();

        final MenuItem result = menuItems.get(name);

        assert result.getName().equals(name);
        return result;
    }

    /**
     * Returns all the menu items of the restaurant.
     *
     * @return list of menu items.
     * @pre true
     * @post @nochange
     */
    @Override
    public List<MenuItem> getAllMenuItems() {
        assert isWellFormed();
        return List.copyOf(menuItems.values());
    }

    /**
     * Verifies whether the given menu item exists in the restaurant.
     *
     * @param menuItem menu item to be verified.
     * @return true if the item exists in the restaurant.
     * @pre menuItem != null
     * @pre menuItem.getName() != null
     * @post @nochange
     */
    @Override
    public boolean containsMenuItem(MenuItem menuItem) {
        assert menuItem != null : "Null argument";
        assert menuItem.getName() != null : "Null argument";
        assert isWellFormed();

        return this.menuItems.containsKey(menuItem.getName());
    }

    /**
     * Verifies whether a menu item exists in the restaurant, having hte given name.
     *
     * @param name name to be verified.
     * @return true if an item exists in the restaurant with the given name.
     * @pre name != null
     * @post @nochange
     */
    @Override
    public boolean containsMenuItem(String name) {
        assert name != null : "Name is null";
        assert isWellFormed();

        return menuItems.containsKey(name);
    }

    /**
     * Places a new order in the restaurant. Observers are notified.
     *
     * @param order     order to be placed.
     * @param menuItems menu items of the order.
     * @pre order != null
     * @pre order.getDate != null
     * @pre menuItems != null
     * @pre items.size() &gt; 0
     * @pre @forall i : [0 .. items.size() - 1] @ containsMenuItem(items.get(i))
     * @post getAllOrders().size() == getAllOrders().size()@pre + 1
     */
    @Override
    public void createOrder(Order order, Collection<MenuItem> menuItems) {
        assert order != null : "order is null";
        assert order.getDate() != null : "date of order is null";
        assert menuItems != null : "menuItems is null";
        assert menuItems.size() > 0 : "an order must have at least one menu item";
        final int sizePre = getAllOrders().size();
        assert isWellFormed();

        for (MenuItem item : menuItems) {
            assert containsMenuItem(item) : "MenuItem with name \"" + item.getName() + "\" does not exists";
        }

        order = new Order(nextOrderId++, order.getDate(), order.getTable());
        orders.put(order, menuItems);
        observable.notifyObservers(List.copyOf(menuItems));

        assert getAllOrders().size() == sizePre + 1;
        assert isWellFormed();
    }

    /**
     * Computes the price of an existing order.
     *
     * @param order order whose price are required to be calculated.
     * @return the total sum of the items of the order.
     * @pre order != null
     * @pre containsOrder(order)
     * @post @nochange
     */
    @Override
    public double computeOrderPrice(Order order) {
        assert order != null : "order is null";
        assert containsOrder(order) : "Order does not exists";
        assert isWellFormed();

        return orders.get(order).stream()
                .mapToDouble(MenuItem::computePrice)
                .sum();
    }

    /**
     * Generates a bill for the given order.
     *
     * @param order order for which the bill is calculated.
     * @pre order != null
     * @pre containsOrder(order)
     * @post @nochange
     */
    @Override
    public void generateBill(Order order) {
        assert order != null : "order is null";
        assert containsOrder(order) : "Order does not exists";
        assert isWellFormed();

        billGenerator.generateBill(order, orders.get(order), computeOrderPrice(order));
    }

    /**
     * Returns all orders of the restaurant.
     *
     * @return list of orders of the restaurant.
     * @pre true
     * @post @nochange
     */
    @Override
    public List<Order> getAllOrders() {
        assert isWellFormed();
        return List.copyOf(orders.keySet());
    }

    /**
     * Verifies whether an order exists in the restaurant.
     *
     * @param order order to be verified.
     * @return true if the order is present in the restaurant.
     * @pre order != null
     * @post @nochange
     */
    @Override
    public boolean containsOrder(Order order) {
        assert order != null : "order is null";
        assert isWellFormed();
        return orders.containsKey(order);
    }

    /**
     * Determines whether the object is well formed and the class invariant holds.
     *
     * @return true if the object is well formed.
     */
    protected boolean isWellFormed() {
        for (Map.Entry<Order, Collection<MenuItem>> entry : orders.entrySet()) {
            final Order order = entry.getKey();
            final Collection<MenuItem> item = entry.getValue();
            if (order == null || order.getId() < 1 || order.getId() >= nextOrderId || null == order.getDate() || order.getTable() < 0) {
                return false;
            }
            if (item == null) {
                return false;
            }
        }

        for (MenuItem item : menuItems.values()) {
            if (item.getName() == null || item.getName().isEmpty() || item.computePrice() < 0) {
                return false;
            }
        }
        return true;
    }
}
