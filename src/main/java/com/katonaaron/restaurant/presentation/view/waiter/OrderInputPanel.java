package com.katonaaron.restaurant.presentation.view.waiter;

import com.katonaaron.restaurant.business.Order;

import javax.swing.*;
import java.awt.*;
import java.util.Date;

public class OrderInputPanel extends JPanel {
    private final JSpinner table = new JSpinner(new SpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
    private final JLabel tableLabel = new JLabel("Table");

    private final GridBagConstraints constraints = new GridBagConstraints();

    public OrderInputPanel() {
        super(new GridBagLayout());

        addInputFields();
    }

    public Order getOrder() {
        return new Order(new Date(), Integer.parseInt(table.getValue().toString()));
    }

    public void reset() {
        table.setValue(0);
    }

    private void addInputFields() {
        constraints.anchor = GridBagConstraints.EAST;
        constraints.weightx = 1;
        constraints.gridx = 1;
        constraints.gridy = 1;
        ((JSpinner.DefaultEditor) table.getEditor()).getTextField().setColumns(17);
        this.add(table, constraints);

        constraints.anchor = GridBagConstraints.WEST;
        constraints.weightx = 0.5;
        constraints.insets = new Insets(0, 0, 0, 20);
        constraints.gridx = 0;
        constraints.gridy = 1;
        tableLabel.setLabelFor(table);
        this.add(tableLabel, constraints);

    }
}
