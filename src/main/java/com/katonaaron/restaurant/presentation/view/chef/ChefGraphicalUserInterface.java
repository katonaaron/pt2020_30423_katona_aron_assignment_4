package com.katonaaron.restaurant.presentation.view.chef;

import com.katonaaron.restaurant.business.menu.MenuItem;
import com.katonaaron.restaurant.observer.Observer;
import com.katonaaron.restaurant.presentation.view.ItemListPanel;

import javax.swing.*;
import java.awt.*;
import java.util.Collection;
import java.util.stream.Collectors;

public class ChefGraphicalUserInterface extends JFrame implements Observer<Collection<MenuItem>> {
    private final JPanel mainPanel = new JPanel(new GridBagLayout());
    private final ItemListPanel itemList = new ItemListPanel();

    private final GridBagConstraints constraints = new GridBagConstraints();

    public ChefGraphicalUserInterface(String title) {
        super(title);

        addPanels();

        this.add(mainPanel);
    }

    private void addPanels() {
        mainPanel.add(itemList, constraints);
    }

    @Override
    public void update(Collection<MenuItem> menuItems) {
        itemList.addItems(menuItems.stream()
                .map(MenuItem::getBaseItems)
                .flatMap(Collection::stream)
                .collect(Collectors.toList())
        );
    }
}
