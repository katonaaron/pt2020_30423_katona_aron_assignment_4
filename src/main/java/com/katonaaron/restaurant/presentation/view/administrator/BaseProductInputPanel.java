package com.katonaaron.restaurant.presentation.view.administrator;

import com.katonaaron.restaurant.business.menu.BaseProduct;
import com.katonaaron.restaurant.business.menu.MenuItem;

import javax.swing.*;
import java.awt.*;

public class BaseProductInputPanel extends JPanel {
    private final JTextField itemName = new JTextField(20);
    private final JLabel itemNameLabel = new JLabel("Item name");
    private final JSpinner itemPrice = new JSpinner(new SpinnerNumberModel(0.0, 0.0, Double.MAX_VALUE, 0.01));
    private final JTextField itemPriceTextField;
    private final JLabel itemPriceLabel = new JLabel("Item price");

    private final GridBagConstraints constraints = new GridBagConstraints();

    public BaseProductInputPanel() {
        super(new GridBagLayout());
        itemPriceTextField = ((JSpinner.DefaultEditor) itemPrice.getEditor()).getTextField();

        addInputFields();
    }

    public BaseProduct getProduct() {
        return new BaseProduct(itemName.getText(), Double.parseDouble(itemPrice.getValue().toString()));
    }

    public void setProduct(MenuItem item) {
        itemName.setText(item.getName());
        itemPrice.setValue(item.computePrice());
    }

    public void setPriceEditable(boolean isEditable) {
        itemPriceTextField.setEditable(isEditable);
    }

    public void clear() {
        itemName.setText("");
        itemPrice.setValue(0);
    }

    private void addInputFields() {
        // Add labels
        constraints.anchor = GridBagConstraints.WEST;
        constraints.weightx = 0.5;
        constraints.insets = new Insets(0, 0, 0, 20);

        constraints.gridx = 0;
        constraints.gridy = 0;
        itemNameLabel.setLabelFor(itemName);
        this.add(itemNameLabel, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        itemPriceLabel.setLabelFor(itemPrice);
        this.add(itemPriceLabel, constraints);


        // Add input fields
        constraints.anchor = GridBagConstraints.EAST;
        constraints.weightx = 1;
        constraints.insets = new Insets(0, 0, 0, 0);

        constraints.gridx = 1;
        constraints.gridy = 0;
        this.add(itemName, constraints);

        constraints.gridx = 1;
        constraints.gridy = 1;
        ((JSpinner.DefaultEditor) itemPrice.getEditor()).getTextField().setColumns(17);
        this.add(itemPrice, constraints);
    }
}
