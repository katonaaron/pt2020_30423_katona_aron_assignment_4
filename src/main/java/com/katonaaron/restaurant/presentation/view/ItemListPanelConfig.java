package com.katonaaron.restaurant.presentation.view;

import java.io.Serializable;

public class ItemListPanelConfig implements Serializable {
    private static final long serialVersionUID = 8995566396864138516L;

    private final boolean quantityShown;
    private final boolean distinctItems;
    private final boolean isEditable;

    public ItemListPanelConfig(boolean quantityShown, boolean distinctItems, boolean isEditable) {
        this.quantityShown = quantityShown;
        this.distinctItems = distinctItems;
        this.isEditable = isEditable;
    }

    public boolean isQuantityShown() {
        return quantityShown;
    }

    public boolean isDistinctItems() {
        return distinctItems;
    }

    public boolean isEditable() {
        return isEditable;
    }
}
