package com.katonaaron.restaurant.presentation.view.administrator;

import com.katonaaron.restaurant.business.menu.MenuItem;
import com.katonaaron.restaurant.business.menu.ProductPriceType;
import com.katonaaron.restaurant.business.menu.ProductType;
import com.katonaaron.restaurant.presentation.view.ItemListPanel;
import com.katonaaron.restaurant.presentation.view.ItemListPanelConfig;
import com.katonaaron.restaurant.presentation.view.RadioButtonOption;
import com.katonaaron.restaurant.presentation.view.RadioButtonPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class EditItemPanel extends JPanel {
    private final RadioButtonPanel productTypeSelectPanel;
    private final RadioButtonPanel productPriceTypeSelectPanel;
    private final BaseProductInputPanel baseProductInputPanel = new BaseProductInputPanel();
    private final ItemListPanel itemListPanel = new ItemListPanel(new ItemListPanelConfig(false, true, false));

    private final GridBagConstraints constraints = new GridBagConstraints();

    public EditItemPanel() {
        super(new GridBagLayout());

        List<RadioButtonOption> options = Arrays.stream(ProductType.values())
                .map(type -> new RadioButtonOption(type.toString(), type.getDescription()))
                .collect(Collectors.toList());
        productTypeSelectPanel = new RadioButtonPanel("Select the item type", options);


        options = Arrays.stream(ProductPriceType.values())
                .map(type -> new RadioButtonOption(type.toString(), type.toString().toLowerCase()))
                .collect(Collectors.toList());
        productPriceTypeSelectPanel = new RadioButtonPanel("Select the price type", options);

        addPanels();
    }

    public void addProductTypeSelectionListener(ActionListener listener) {
        productTypeSelectPanel.addActionListener(listener);
    }

    public void addProductPriceTypeSelectionListener(ActionListener listener) {
        productPriceTypeSelectPanel.addActionListener(listener);
    }

    public void setProductType(ProductType type) {
        productTypeSelectPanel.setOption(type.toString());
    }

    public void setProductPriceType(ProductPriceType type) {
        productPriceTypeSelectPanel.setOption(type.toString());
    }

    public MenuItem getBaseProduct() {
        return baseProductInputPanel.getProduct();
    }

    public void setBaseProduct(MenuItem item) {
        baseProductInputPanel.setProduct(item);
    }

    public List<String> getSelectedComponentNames() {
        return itemListPanel.getSelectedItems();
    }

    public void selectComponents(List<MenuItem> menuItems) {
        itemListPanel.selectItems(menuItems.stream()
                .map(MenuItem::getName)
                .collect(Collectors.toList())
        );
    }

    public void clearComponentSelection() {
        itemListPanel.clearSelection();
    }

    public void setCompositeInputEnabled(boolean enabled) {
        productPriceTypeSelectPanel.setEnabled(enabled);
        itemListPanel.setEnabled(enabled);
    }

    public void setProductTypeSelectEnabled(boolean enabled) {
        productTypeSelectPanel.setEnabled(enabled);
    }

    public void reset() {
        baseProductInputPanel.clear();
        productTypeSelectPanel.reset();
        productTypeSelectPanel.setEnabled(true);
        productPriceTypeSelectPanel.reset();
        itemListPanel.clear();
    }

    public void refreshItemList(List<MenuItem> itemList) {
        itemListPanel.clear();
        itemListPanel.addItems(itemList);
    }

    public void setBaseItem(MenuItem item) {
        baseProductInputPanel.setProduct(item);
    }

    public void setPriceEditable(boolean editable) {
        baseProductInputPanel.setPriceEditable(editable);
    }

    private void addPanels() {
        constraints.weightx = constraints.weighty = 1.0;

        constraints.gridx = 0;
        constraints.gridy = 0;
        this.add(baseProductInputPanel, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        this.add(itemListPanel, constraints);

        constraints.gridx = 1;
        constraints.gridy = 0;
        this.add(productTypeSelectPanel, constraints);

        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.ipadx = 70;
        this.add(productPriceTypeSelectPanel, constraints);
    }
}
