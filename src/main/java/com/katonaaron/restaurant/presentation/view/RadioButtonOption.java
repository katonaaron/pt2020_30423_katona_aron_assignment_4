package com.katonaaron.restaurant.presentation.view;

public class RadioButtonOption {
    private final String actionCommand;
    private final String label;

    public RadioButtonOption(String actionCommand, String label) {
        this.actionCommand = actionCommand;
        this.label = label;
    }

    public String getActionCommand() {
        return actionCommand;
    }

    public String getLabel() {
        return label;
    }
}
