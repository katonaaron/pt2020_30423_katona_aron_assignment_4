package com.katonaaron.restaurant.presentation.view.waiter;

import com.katonaaron.restaurant.business.Order;
import com.katonaaron.restaurant.business.menu.MenuItem;
import com.katonaaron.restaurant.presentation.view.ItemListPanel;
import com.katonaaron.restaurant.presentation.view.ItemListPanelConfig;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class CreateOrderPanel extends JPanel {
    private final ItemListPanel itemListPanel = new ItemListPanel(new ItemListPanelConfig(false, true, false));
    private final OrderInputPanel orderInputPanel = new OrderInputPanel();

    private final GridBagConstraints constraints = new GridBagConstraints();

    public CreateOrderPanel() {
        super(new GridBagLayout());

        addPanels();
    }

    public void reset() {
        orderInputPanel.reset();
        itemListPanel.clear();
    }

    public void refreshItemList(List<MenuItem> itemList) {
        itemListPanel.clear();
        itemListPanel.addItems(itemList);
    }

    private void addPanels() {
        constraints.weightx = constraints.weighty = 1.0;

        constraints.gridx = 0;
        constraints.gridy = 0;
        this.add(orderInputPanel, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        this.add(itemListPanel, constraints);
    }

    public Order getOrder() {
        return orderInputPanel.getOrder();
    }

    public List<String> getSelectedItems() {
        return itemListPanel.getSelectedItems();
    }
}
