package com.katonaaron.restaurant.presentation.view.waiter;

import com.katonaaron.restaurant.business.Order;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class OrderListPanel extends JPanel {
    private final GridBagConstraints constraints = new GridBagConstraints();
    private final String[] columnNames;

    private JTable table;
    private DefaultTableModel tableModel;
    private JScrollPane scrollPane;

    public OrderListPanel() {
        super(new GridBagLayout());

        columnNames = new String[]{"Id", "Date", "Table"};

        addTable();
    }

    public void addOrders(List<Order> orders) {
        orders.stream()
                .map(order -> new Object[]{order.getId(), order.getDate(), order.getTable()})
                .forEach(tableModel::addRow);
    }

    public void clear() {
        tableModel.setRowCount(0);
    }

    public List<Order> getSelectedOrders() {
        return Arrays.stream(table.getSelectedRows())
                .mapToObj(rowIndex -> new Order(
                        (int) table.getValueAt(rowIndex, 0),
                        (Date) table.getValueAt(rowIndex, 1),
                        (int) table.getValueAt(rowIndex, 2))
                )
                .collect(Collectors.toList());
    }

    public void clearSelection() {
        table.clearSelection();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        table.setEnabled(enabled);
        clearSelection();
    }

    private void addTable() {
        tableModel = new DefaultTableModel(columnNames, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        table = new JTable(tableModel);
        scrollPane = new JScrollPane(table);

        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

        this.add(scrollPane);
    }
}
