package com.katonaaron.restaurant.presentation.view;

import com.katonaaron.restaurant.business.menu.MenuItem;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class ItemListPanel extends JPanel {
    public static final ItemListPanelConfig DEFAULT_CONFIG
            = new ItemListPanelConfig(true, false, false);

    private final GridBagConstraints constraints = new GridBagConstraints();
    private final String[] columnNames;
    private final ItemListPanelConfig config;

    private JTable table;
    private DefaultTableModel tableModel;
    private JScrollPane scrollPane;

    public ItemListPanel() {
        this(DEFAULT_CONFIG);
    }

    public ItemListPanel(ItemListPanelConfig config) {
        super(new GridBagLayout());

        this.config = config;

        if (config.isQuantityShown()) {
            columnNames = new String[]{"Name", "Price", "Quantity"};
        } else {
            columnNames = new String[]{"Name", "Price"};
        }

        addTable();
    }

    public void addItems(List<MenuItem> items) {
        listToRows(items)
                .forEach(tableModel::addRow);
    }

    public void clear() {
        tableModel.setRowCount(0);
    }

    public List<String> getSelectedItems() {
        return Arrays.stream(table.getSelectedRows())
                .mapToObj(rowIndex -> (String) table.getValueAt(rowIndex, 0))
                .collect(Collectors.toList());
    }

    public void selectItems(List<String> itemNames) {
        final Set<String> itemSet = new HashSet<>(itemNames);
        for (int i = 0; i < table.getRowCount(); i++) {
            if (itemSet.contains(tableModel.getValueAt(i, 0))) {
                table.addRowSelectionInterval(i, i);
            }
        }
    }

    public void clearSelection() {
        table.clearSelection();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        table.setEnabled(enabled);
        clearSelection();
    }

    private void addTable() {
        tableModel = new DefaultTableModel(columnNames, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return config.isEditable();
            }
        };
        table = new JTable(tableModel);
        scrollPane = new JScrollPane(table);

        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

        this.add(scrollPane);
    }

    private List<Object[]> listToRows(List<MenuItem> items) {
        if (config.isQuantityShown()) {
            return items.stream()
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                    .entrySet()
                    .stream()
                    .map(entry -> List.of(entry.getKey().getName(), entry.getKey().computePrice(), entry.getValue()).toArray())
                    .collect(Collectors.toList());
        }

        Stream<MenuItem> resultStream = items.stream();
        if (config.isDistinctItems()) {
            Set<String> existingNames = getExistingNames();
            resultStream = resultStream
                    .distinct()
                    .filter(item -> !existingNames.contains(item.getName()));
        }
        return resultStream.map(item -> List.of(item.getName(), item.computePrice()).toArray()).collect(Collectors.toList());
    }

    private Set<String> getExistingNames() {
        return IntStream.range(0, table.getRowCount())
                .mapToObj(rowIndex -> (String) table.getValueAt(rowIndex, 0))
                .collect(Collectors.toSet());
    }
}
