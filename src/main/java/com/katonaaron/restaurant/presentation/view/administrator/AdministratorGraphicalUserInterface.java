package com.katonaaron.restaurant.presentation.view.administrator;

import com.katonaaron.restaurant.business.menu.MenuItem;
import com.katonaaron.restaurant.presentation.view.ItemListPanel;
import com.katonaaron.restaurant.presentation.view.ItemListPanelConfig;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

public class AdministratorGraphicalUserInterface extends JFrame {
    private final JPanel mainPanel = new JPanel(new GridBagLayout());
    private final ItemListPanel itemListPanel = new ItemListPanel(new ItemListPanelConfig(false, true, false));
    private final EditItemPanel editItemPanel;
    private final JButton createButton = new JButton("Create a new item");
    private final JButton deleteButton = new JButton("Delete items");
    private final JButton editButton = new JButton("Edit an item");
    private final JButton saveButton = new JButton("Save restaurant");
    private final GridBagConstraints constraints = new GridBagConstraints(
            0,
            0,
            1,
            1,
            1.0,
            1.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.NONE,
            new Insets(0, 0, 0, 0),
            0,
            0
    );

    public AdministratorGraphicalUserInterface(String title, EditItemPanel editItemPanel) {
        super(title);
        this.editItemPanel = editItemPanel;

        addPanels();

        this.add(mainPanel);
    }

    public void addCreateButtonListener(ActionListener listener) {
        createButton.addActionListener(listener);
    }

    public void addDeleteButtonListener(ActionListener listener) {
        deleteButton.addActionListener(listener);
    }

    public void addEditButtonListener(ActionListener listener) {
        editButton.addActionListener(listener);
    }

    public void addSaveButtonListener(ActionListener listener) {
        saveButton.addActionListener(listener);
    }

    public List<String> getSelectedItems() {
        return itemListPanel.getSelectedItems();
    }

    public int showEditDialog() {
        return JOptionPane.showConfirmDialog(
                null,
                editItemPanel,
                "Create a new item",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE
        );
    }

    public void refreshItemList(List<MenuItem> itemList) {
        itemListPanel.clear();
        itemListPanel.addItems(itemList);
    }

    private void addPanels() {
        constraints.gridx = 1;
        constraints.gridy = 0;
        mainPanel.add(createButton, constraints);

        constraints.gridx = 1;
        constraints.gridy = 1;
        mainPanel.add(editButton, constraints);

        constraints.gridx = 1;
        constraints.gridy = 2;
        mainPanel.add(deleteButton, constraints);

        constraints.gridx = 1;
        constraints.gridy = 3;
        mainPanel.add(saveButton, constraints);

        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridheight = 4;
        mainPanel.add(itemListPanel, constraints);
    }
}
