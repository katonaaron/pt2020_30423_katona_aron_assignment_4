package com.katonaaron.restaurant.presentation.view.waiter;

import com.katonaaron.restaurant.business.Order;
import com.katonaaron.restaurant.business.menu.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

public class WaiterGraphicalUserInterface extends JFrame {
    private final JPanel mainPanel = new JPanel(new GridBagLayout());
    private final OrderListPanel orderListPanel = new OrderListPanel();
    private final CreateOrderPanel createOrderPanel = new CreateOrderPanel();
    private final JButton createButton = new JButton("Create a new order");
    private final JButton generateBillButton = new JButton("Generate a bill");
    private final JButton calculatePriceButton = new JButton("Calculate price");

    private final GridBagConstraints constraints = new GridBagConstraints(
            0,
            0,
            1,
            1,
            1.0,
            1.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.NONE,
            new Insets(0, 0, 0, 0),
            0,
            0
    );

    public WaiterGraphicalUserInterface(String title) {
        super(title);

        addPanels();

        this.add(mainPanel);
    }

    public void addCreateButtonListener(ActionListener listener) {
        createButton.addActionListener(listener);
    }

    public void addGenerateBillButtonListener(ActionListener listener) {
        generateBillButton.addActionListener(listener);
    }

    public void addCalculatePriceButtonListener(ActionListener listener) {
        calculatePriceButton.addActionListener(listener);
    }

    public List<Order> getSelectedOrders() {
        return orderListPanel.getSelectedOrders();
    }

    public void refreshOrderList(List<Order> orderList) {
        orderListPanel.clear();
        orderListPanel.addOrders(orderList);
    }

    public void resetCreateDialog() {
        createOrderPanel.reset();
    }

    public void refreshMenuItems(List<MenuItem> menuItems) {
        createOrderPanel.refreshItemList(menuItems);
    }

    public int showCreateDialog() {
        return JOptionPane.showConfirmDialog(
                null,
                createOrderPanel,
                "Create a new order",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE
        );
    }

    public Order getCreateDialogOrder() {
        return createOrderPanel.getOrder();
    }

    private void addPanels() {
        constraints.gridx = 1;
        constraints.gridy = 0;
        mainPanel.add(createButton, constraints);

        constraints.gridx = 1;
        constraints.gridy = 1;
        mainPanel.add(calculatePriceButton, constraints);

        constraints.gridx = 1;
        constraints.gridy = 2;
        mainPanel.add(generateBillButton, constraints);

        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridheight = 3;
        mainPanel.add(orderListPanel, constraints);
    }

    public List<String> getCreateDialogSelectedItems() {
        return createOrderPanel.getSelectedItems();
    }
}
