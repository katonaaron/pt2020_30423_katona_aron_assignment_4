package com.katonaaron.restaurant.presentation.view;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.NoSuchElementException;

public class RadioButtonPanel extends JPanel {
    private final List<JRadioButton> buttonList = new ArrayList<>();
    private final ButtonGroup buttonGroup = new ButtonGroup();

    private final GridBagConstraints constraints = new GridBagConstraints(
            0,
            0,
            GridBagConstraints.RELATIVE,
            1,
            1.0,
            1.0,
            GridBagConstraints.CENTER,
            GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0),
            0,
            0
    );

    public RadioButtonPanel(String title, List<RadioButtonOption> options) {
        super(new GridBagLayout());

        this.setBorder(new TitledBorder(title));

        addRadioButtons(options);
    }

    public String getSelectedActionCommand() {
        return buttonGroup.getSelection().getActionCommand();
    }

    public void setOption(String actionCommand) {
        final Enumeration<AbstractButton> buttons = buttonGroup.getElements();
        while (buttons.hasMoreElements()) {
            final AbstractButton button = buttons.nextElement();
            if (button.getActionCommand().equals(actionCommand)) {
                button.doClick();
                return;
            }
        }
        throw new NoSuchElementException("Option with action command \"" + actionCommand + "\" does not exists");
    }

    public void addActionListener(ActionListener listener) {
        buttonList.forEach(button -> button.addActionListener(listener));
    }

    public void reset() {
        buttonGroup.getElements().nextElement().doClick();
    }

    @Override
    public void setEnabled(boolean enabled) {
        if (!isEnabled() && enabled) {
            reset();
        }
        super.setEnabled(enabled);
        buttonList.forEach(button -> button.setEnabled(enabled));
    }

    private void addRadioButtons(List<RadioButtonOption> options) {
        for (RadioButtonOption option : options) {
            JRadioButton button = new JRadioButton(option.getLabel());
            button.setActionCommand(option.getActionCommand());

            buttonList.add(button);
            buttonGroup.add(button);
            this.add(button, constraints);
            constraints.gridy++;
        }

        reset();
    }
}
