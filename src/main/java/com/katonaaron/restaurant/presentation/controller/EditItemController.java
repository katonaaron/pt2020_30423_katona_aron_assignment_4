package com.katonaaron.restaurant.presentation.controller;

import com.katonaaron.restaurant.business.IRestaurantProcessing;
import com.katonaaron.restaurant.business.menu.CompositeProduct;
import com.katonaaron.restaurant.business.menu.MenuItem;
import com.katonaaron.restaurant.business.menu.ProductPriceType;
import com.katonaaron.restaurant.business.menu.ProductType;
import com.katonaaron.restaurant.presentation.view.administrator.EditItemPanel;

import java.awt.event.ActionEvent;
import java.util.List;
import java.util.stream.Collectors;

public class EditItemController {
    private final EditItemPanel editItemPanel;
    private final IRestaurantProcessing restaurant;
    private ProductType productType;
    private ProductPriceType priceType;

    public EditItemController(EditItemPanel editItemPanel, IRestaurantProcessing restaurant) {
        this.editItemPanel = editItemPanel;
        this.restaurant = restaurant;
        this.productType = ProductType.values()[0];
        this.priceType = ProductPriceType.values()[0];

        editItemPanel.addProductTypeSelectionListener(this::typeSelected);
        editItemPanel.addProductPriceTypeSelectionListener(this::priceTypeSelected);

        prepareGUI();
    }

    public MenuItem getMenuItem() {
        switch (productType) {
            case BASE:
                return editItemPanel.getBaseProduct();
            case COMPOSITE:
                return getCompositeProduct();
            default:
                throw new IllegalStateException("Unexpected value: " + productType);
        }
    }

    public void prepareGUI() {
        editItemPanel.reset();
        refreshItemList();
    }

    public void loadItem(MenuItem item) {
        if (item == null) {
            throw new IllegalArgumentException("item is null");
        }

        productType = item.getType();
        priceType = item.getPriceType();
        typeSelected();

        editItemPanel.setProductType(productType);
        editItemPanel.setProductPriceType(priceType);

        //Disable product type change
        editItemPanel.setProductTypeSelectEnabled(false);

        editItemPanel.setBaseItem(item);
        if (productType == ProductType.COMPOSITE) {
            editItemPanel.selectComponents(((CompositeProduct) item).getComponents());
        }
    }

    private CompositeProduct getCompositeProduct() {
        final MenuItem base = editItemPanel.getBaseProduct();
        CompositeProduct composite;

        switch (priceType) {
            case AUTOMATIC:
                composite = new CompositeProduct(base.getName());
                break;
            case EXPLICIT:
                composite = new CompositeProduct(base.getName(), base.computePrice());
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + priceType);
        }

        final List<MenuItem> components = editItemPanel.getSelectedComponentNames().stream()
                .map(restaurant::getMenuItemByName)
                .collect(Collectors.toList());

        composite.add(components);
        return composite;
    }

    private void typeSelected(ActionEvent event) {
        productType = ProductType.valueOf(event.getActionCommand());
        typeSelected();
    }

    private void typeSelected() {
        editItemPanel.setCompositeInputEnabled(productType == ProductType.COMPOSITE);
        priceTypeSelected();
    }

    private void priceTypeSelected(ActionEvent event) {
        priceType = ProductPriceType.valueOf(event.getActionCommand());
        priceTypeSelected();
    }

    private void priceTypeSelected() {
        switch (productType) {
            case BASE:
                editItemPanel.setPriceEditable(true);
                break;
            case COMPOSITE:
                switch (priceType) {
                    case AUTOMATIC:
                        editItemPanel.setPriceEditable(false);
                        break;
                    case EXPLICIT:
                        editItemPanel.setPriceEditable(true);
                        break;
                    default:
                        throw new IllegalStateException("Unexpected value: " + priceType);
                }
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + productType);
        }
    }

    private void refreshItemList() {
        editItemPanel.refreshItemList(restaurant.getAllMenuItems());
    }
}
