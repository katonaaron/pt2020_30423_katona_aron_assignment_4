package com.katonaaron.restaurant.presentation.controller;

import com.katonaaron.restaurant.business.IRestaurantProcessing;
import com.katonaaron.restaurant.business.Order;
import com.katonaaron.restaurant.business.menu.MenuItem;
import com.katonaaron.restaurant.exception.BillGenerationException;
import com.katonaaron.restaurant.presentation.view.waiter.WaiterGraphicalUserInterface;

import javax.swing.*;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class WaiterController {
    private final WaiterGraphicalUserInterface waiterGUI;
    private final IRestaurantProcessing restaurant;

    public WaiterController(WaiterGraphicalUserInterface waiterGUI, IRestaurantProcessing restaurant) {
        this.waiterGUI = waiterGUI;
        this.restaurant = restaurant;

        waiterGUI.addCreateButtonListener(event -> createOrder());
        waiterGUI.addGenerateBillButtonListener(event -> generateBill());
        waiterGUI.addCalculatePriceButtonListener(event -> calculatePrice());

        refreshOrderList();
    }

    private void calculatePrice() {
        final List<Order> selectedOrders = waiterGUI.getSelectedOrders();
        if (selectedOrders.size() != 1) {
            showErrorDialog("Please select a single order for calculating the price");
            return;
        }

        final Order order = selectedOrders.get(0);
        final double total = restaurant.computeOrderPrice(order);
        JOptionPane.showMessageDialog(null, "Total sum of order \"" + order.getId() + "\" is: " + total);
    }

    private void generateBill() {
        final List<Order> selectedOrders = waiterGUI.getSelectedOrders();
        if (selectedOrders.size() != 1) {
            showErrorDialog("Please select a single order for bill generation");
            return;
        }

        try {
            final Order order = selectedOrders.get(0);
            restaurant.generateBill(order);
            JOptionPane.showMessageDialog(null, "Bill generated for order with id \"" + order.getId() + "\"");
        } catch (BillGenerationException e) {
            showErrorDialog(e.getMessage());
        }
    }

    private void createOrder() {
        waiterGUI.resetCreateDialog();
        waiterGUI.refreshMenuItems(restaurant.getAllMenuItems());

        if (waiterGUI.showCreateDialog() == JOptionPane.OK_OPTION) {
            final Order order = waiterGUI.getCreateDialogOrder();
            final List<MenuItem> selectedItems = waiterGUI
                    .getCreateDialogSelectedItems()
                    .stream()
                    .map(restaurant::getMenuItemByName)
                    .collect(Collectors.toList());

            restaurant.createOrder(order, selectedItems);
            JOptionPane.showMessageDialog(null, "order added successfully");
            refreshOrderList();
        }
    }

    private void refreshOrderList() {
        waiterGUI.refreshOrderList(
                restaurant.getAllOrders().stream()
                        .sorted(Comparator.comparingInt(Order::getId))
                        .collect(Collectors.toList())
        );
    }

    private void showErrorDialog(String message) {
        JOptionPane optionPane = new JOptionPane(message, JOptionPane.ERROR_MESSAGE);
        JDialog dialog = optionPane.createDialog("Error");
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }
}
