package com.katonaaron.restaurant.presentation.controller;

import com.katonaaron.restaurant.business.IRestaurantProcessing;
import com.katonaaron.restaurant.business.menu.MenuItem;
import com.katonaaron.restaurant.data.Serializator;
import com.katonaaron.restaurant.exception.SerializationException;
import com.katonaaron.restaurant.presentation.view.administrator.AdministratorGraphicalUserInterface;

import javax.swing.*;
import java.util.List;

public class AdministratorController {
    private final AdministratorGraphicalUserInterface adminGUI;
    private final EditItemController editItemController;
    private final IRestaurantProcessing restaurant;
    private final Serializator<IRestaurantProcessing> serializator;

    public AdministratorController(AdministratorGraphicalUserInterface adminGUI, EditItemController editItemController, IRestaurantProcessing restaurant, Serializator<IRestaurantProcessing> serializator) {
        this.adminGUI = adminGUI;
        this.editItemController = editItemController;
        this.restaurant = restaurant;
        this.serializator = serializator;

        adminGUI.addCreateButtonListener(e -> this.createItem());
        adminGUI.addEditButtonListener(e -> this.editItem());
        adminGUI.addDeleteButtonListener(e -> this.deleteItems());
        adminGUI.addSaveButtonListener(e -> this.serializeRestaurant());
        refreshItemList();
    }

    private void serializeRestaurant() {
        try {
            serializator.serialize(restaurant);
            JOptionPane.showMessageDialog(null, "Restaurant saved successfully");
        } catch (SerializationException e) {
            showErrorDialog("Could not save menu items: " + e.getMessage());
        }
    }

    private void createItem() {
        editItemController.prepareGUI();
        final int result = adminGUI.showEditDialog();

        if (result == JOptionPane.OK_OPTION) {
            final MenuItem item = editItemController.getMenuItem();
            if (item.getName().isEmpty()) {
                showErrorDialog("Item name must not be empty");
                return;
            }
            if (restaurant.containsMenuItem(item.getName())) {
                showErrorDialog("Item with name \"" + item.getName() + "\" already exists");
                return;
            }
            restaurant.createMenuItem(item);
            JOptionPane.showMessageDialog(null, item.getName() + " added successfully");
            refreshItemList();
        }
    }

    private void editItem() {
        editItemController.prepareGUI();

        final List<String> selectedItems = adminGUI.getSelectedItems();
        if (selectedItems.size() != 1) {
            showErrorDialog("Please select a single item to edit");
            return;
        }

        String selectedName = selectedItems.get(0);
        if (!restaurant.containsMenuItem(selectedName)) {
            showErrorDialog("MenuItem with name \"" + selectedName + "\" does not exists");
            return;
        }
        final MenuItem oldItem = restaurant.getMenuItemByName(selectedName);
        editItemController.loadItem(oldItem);

        if (adminGUI.showEditDialog() == JOptionPane.OK_OPTION) {
            final MenuItem newItem = editItemController.getMenuItem();

            if (oldItem.getName().equals(newItem.getName())) {
                restaurant.updateMenuItem(newItem);
            } else {
                restaurant.updateMenuItem(oldItem.getName(), newItem);
            }
            JOptionPane.showMessageDialog(null, newItem.getName() + " modified successfully");
            refreshItemList();
        }
    }

    private void deleteItems() {
        final List<String> items = adminGUI.getSelectedItems();
        for (String itemName : items) {
            if (!restaurant.containsMenuItem(itemName)) {
                showErrorDialog("MenuItem with name \"" + itemName + "\" does not exists");
                return;
            }
        }
        items.forEach(restaurant::deleteMenuItemByName);
        refreshItemList();
        JOptionPane.showMessageDialog(null, "Item(s) deleted successfully");
    }

    private void refreshItemList() {
        adminGUI.refreshItemList(restaurant.getAllMenuItems());
    }

    private void showErrorDialog(String message) {
        JOptionPane optionPane = new JOptionPane(message, JOptionPane.ERROR_MESSAGE);
        JDialog dialog = optionPane.createDialog("Error");
        dialog.setAlwaysOnTop(true);
        dialog.setVisible(true);
    }
}
