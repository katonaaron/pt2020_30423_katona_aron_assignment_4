package com.katonaaron.restaurant.observer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Observable<T> implements Serializable {
    private static final long serialVersionUID = 7308344956627652418L;

    private final List<Observer<T>> observers;

    public Observable() {
        observers = new ArrayList<>();
    }

    public void addObserver(Observer<T> observer) {
        observers.add(observer);
    }

    public void removeObserver(Observer<T> observer) {
        observers.remove(observer);
    }

    public void notifyObservers(T arg) {
        observers.forEach(o -> o.update(arg));
    }
}
