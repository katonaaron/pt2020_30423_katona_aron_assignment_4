package com.katonaaron.restaurant;

import com.katonaaron.restaurant.business.Restaurant;
import com.katonaaron.restaurant.data.FileBillGenerator;
import com.katonaaron.restaurant.data.Serializator;
import com.katonaaron.restaurant.exception.SerializationException;
import com.katonaaron.restaurant.presentation.controller.AdministratorController;
import com.katonaaron.restaurant.presentation.controller.EditItemController;
import com.katonaaron.restaurant.presentation.controller.WaiterController;
import com.katonaaron.restaurant.presentation.view.administrator.AdministratorGraphicalUserInterface;
import com.katonaaron.restaurant.presentation.view.administrator.EditItemPanel;
import com.katonaaron.restaurant.presentation.view.chef.ChefGraphicalUserInterface;
import com.katonaaron.restaurant.presentation.view.waiter.WaiterGraphicalUserInterface;

import javax.swing.*;
import java.io.FileNotFoundException;

public class App {
    private static final String APP_NAME = "Restaurant Management";
    private static final String ICON_PATH = "/icon.png";
    private static final String DEFAULT_SERIALIZATION_FILE_NAME = "restaurant.ser";

    private static ImageIcon icon;
    private static String serializatonFileName;

    private static Restaurant restaurant;
    private static ChefGraphicalUserInterface chefGUI;
    private static AdministratorGraphicalUserInterface adminGUI;
    private static WaiterGraphicalUserInterface waiterGUI;

    public static void main(String[] args) {
        if (args.length > 1) {
            System.err.println("Arguments: [path_to_serialized_restaurant]");
            System.exit(-1);
        } else if (args.length == 1) {
            serializatonFileName = args[0];
        } else {
            serializatonFileName = DEFAULT_SERIALIZATION_FILE_NAME;
        }

        icon = new ImageIcon(App.class.getResource(ICON_PATH));
        final Serializator<Restaurant> restaurantSerializator = new Serializator<>(serializatonFileName);

        try {
            restaurant = restaurantSerializator.deserialize();
        } catch (SerializationException e) {
            if (e.getCause() instanceof FileNotFoundException) {
                restaurant = new Restaurant(new FileBillGenerator());
            } else {
                System.err.println("Could not deserialize \"" + serializatonFileName + "\": " + e.getMessage());
                System.exit(-1);
            }
        }

        instantiate();

        restaurant.addObserver(chefGUI);
        startFrame(chefGUI);
        startFrame(adminGUI);
        startFrame(waiterGUI);
    }

    private static void instantiate() {
        EditItemPanel editItemPanel = new EditItemPanel();

        chefGUI = new ChefGraphicalUserInterface(APP_NAME + " - Chef");
        adminGUI = new AdministratorGraphicalUserInterface(APP_NAME + " - Administrator", editItemPanel);
        waiterGUI = new WaiterGraphicalUserInterface(APP_NAME + " - Waiter");

        EditItemController editItemController = new EditItemController(editItemPanel, restaurant);
        AdministratorController adminController = new AdministratorController(adminGUI, editItemController, restaurant, new Serializator<>(serializatonFileName));
        WaiterController waiterController = new WaiterController(waiterGUI, restaurant);
    }

    private static void startFrame(JFrame frame) {
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setIconImage(icon.getImage());
        frame.setVisible(true);
    }
}
