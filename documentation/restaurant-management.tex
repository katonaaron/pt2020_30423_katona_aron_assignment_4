 
\documentclass[a4paper,10pt]{article}
\usepackage{fontspec}
\usepackage[singlespacing]{setspace}
\usepackage{listings}
\usepackage{color}
\usepackage{graphicx}
\usepackage{hyperref}
\usepackage{todonotes}
\hypersetup{
    colorlinks,
    citecolor=black,
    filecolor=black,
    linkcolor=black,
    urlcolor=black
}
\setmainfont{Times New Roman}

\newcommand{\subsubsubsection}[1]{\paragraph{#1}\mbox{}\\}
\setcounter{secnumdepth}{4}
\setcounter{tocdepth}{4}

%opening
\title{Restaurant Management}
\author{Katona Áron}

\begin{document}

\pagenumbering{gobble}
\maketitle

\newpage
\pagenumbering{roman}
\tableofcontents

\newpage
\pagenumbering{arabic}

\listoftodos

\section{Objective}

The main objective is to  implement a restaurant management system. The system should have three types of users: administrator, waiter and chef. The administrator can add, delete and modify existing products from the menu. The waiter can create a new order for a table, add elements from the menu, and compute the bill for an order. The chef is notified each time it must cook food that is ordered through a waiter.



\subsection{Secondary objectives}

The main objective can be divided into multiple independent steps:

\begin{itemize}
    
\item \textbf{Creating a graphical user interface} 
\hyperref[sec:view]{(\ref{sec:view})}

\item \textbf{Creating a layered architecture}
\hyperref[sec:packages]{(\ref{sec:packages})}

\item \textbf{Providing communication between the GUI and the business layer} 
\hyperref[sec:controller]{(\ref{sec:controller})}

\item \textbf{Creating the appropriate models for storing the data}
\hyperref[sec:menu_item]{(\ref{sec:menu_item})}
\hyperref[sec:order]{(\ref{sec:order})}

\item \textbf{Applying the design by contract principle}
\hyperref[sec:restaurant]{(\ref{sec:restaurant})}

\item \textbf{Generating bills in text files}
\hyperref[sec:bill]{(\ref{sec:bill})}

\item \textbf{Saving and loading the restaurant state by the means of serialization}
\hyperref[sec:serialize]{(\ref{sec:serialize})}

\end{itemize}




\section{Problem analysis and use cases}


\subsection{Problem analysis}
\label{sec:analysis}

The first step to solve the problem is to model the restaurant and the products and implement the necessary operations associated to the three types of employees. For each employee a new graphical user interface is created in a separate window. Then the communication should be implemented between them. Finally, the serialization and bill generation follow.

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=\linewidth]{figures/gui.png}
    \caption{The three graphical user interfaces} 
    \label{fig:gui}
\end{figure*}

\subsection{Input}

The program can be started using the following command:
\begin{verbatim}
java -jar PT2020_30423_Aron_Katona_Assignment_4.jar \
         [<path_to_serialized_restaurant>]
\end{verbatim}
Where \verb|path_to_serialized_restaurant| points to a file generated when the administrator pressed the ``Save restaurant'' button. This an optional parameter. The default filename, when no was given, is: 
\begin{verbatim}
restaurant.ser
\end{verbatim}

\subsection{Output}
If the waiter presses the ``Generate a bill'' button,  a new text file is created having the following name:
\begin{verbatim}
bill-<order_id>.pdf
\end{verbatim}
Where \verb|<order_id>| is the id of the order about which the bill was generated.

If the administrator presses the ``Save restaurant'' button the restaurant will be serialized. The name of the output file is the one given as the command line parameter. The program overwrites an existing file. 



\subsection{Use cases}

\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=\linewidth]{figures/restaurant-use-cases.png}
    \caption{Use cases of the restaurant} 
    \label{fig:restaurant-use-cases}
\end{figure*}

\subsubsection{Create a menu item}
\label{sec:create}

\textbf{Use Case:} Create a menu item\\
\textbf{Primary Actor:} Administrator\\
\textbf{Main Success Scenario:}

\begin{enumerate}
\item \label{item:a1_1} The administrator presses the ``Create a new item'' button.

\item The program shows a dialog box with input fields.

\item The administrator selects the desired type of product: 
\begin{itemize}
 \item Base product
 \item Composite product
\end{itemize}

\item \label{item:a1_2} The administrator inputs the name of the item in the ``Item name'' field.

\item if the selected type is a base product:
\begin{enumerate}

 \item The administrator inputs the price of the item in the ``Item price'' field.
\end{enumerate}
\item If the selected type is a composite product:
\begin{enumerate}

\item The administrator selects how the price should be calculated:
\begin{itemize}

\item Explicit: The price of the composite product will be given explicitly in ``Item price'' field.

\item Automatic: The price of the composite product will be calculated based on the menu items it will contain.

\end{itemize}

\item The administrator inputs the price of the item in the ``Item price'' field if explicit price is selected.

\item The administrator selects the menu items from the table which will be the parts of the composite product

\end{enumerate}

\item The administrator presses the OK button.

\item The program displays a message that the item was successfully created.

\end{enumerate}


\textbf{Alternative Sequences:}
\begin{enumerate}

\item The administrator presses the ``Cancel'' button.
\begin{itemize}
\item The dialog closes.
\item The scenario returns to step \ref{item:a1_1}.
\end{itemize}

\item Item name was not specified.
\begin{itemize}
\item The program shows an error message.
\item The scenario returns to step \ref{item:a1_1}.
\end{itemize}

\item An item with that name already exists.
\begin{itemize}
\item The program shows an error message.
\item The scenario returns to step \ref{item:a1_1}.
\end{itemize}

\end{enumerate}




\subsubsection{Edit a menu item}

\textbf{Use Case:} Edit a menu item\\
\textbf{Primary Actor:} Administrator\\
\textbf{Main Success Scenario:}

\begin{enumerate}
 \item \label{item:a2_1} The administrator presses the ``Edit an item'' button.

\item The program shows a dialog box with input fields preloaded with the data of the selected item.

\item The following steps are identical to the steps of the ``Create an item'' use case [\ref{sec:create}], starting from \ref{item:a1_2}.

\end{enumerate}

\textbf{Alternative Sequences:}

Same as the alternative sequences of the ``Create an item'' use case (\ref{sec:create}). In each case the scenario returns to step \ref{item:a2_1}.




\subsubsection{Delete menu items}

\textbf{Use Case:} Delete menu items\\
\textbf{Primary Actor:} Administrator\\
\textbf{Main Success Scenario:}

\begin{enumerate}

\item \label{item:a3_1} The administrator selects in the table the menu items for deletion.

\item The administrator presses the ``Delete items'' button.

\item The program displays a message that the items were successfully deleted.

\end{enumerate}

\textbf{Alternative Sequences:}
\begin{enumerate}

\item No item was selected before the button was pressed.
\begin{itemize}
\item The program displays an error message.
\item The scenario returns to step \ref{item:a3_1}.
\end{itemize}

\end{enumerate}



\subsubsection{Save restaurant into a file}

\textbf{Use Case:} Save restaurant into a file\\
\textbf{Primary Actor:} Administrator\\
\textbf{Main Success Scenario:}

\begin{enumerate}

\item \label{item:a4_1} The administrator presses the ``Save restaurant'' button.

\item The program displays a message that the restaurant was successfully saved.

\end{enumerate}

\textbf{Alternative Sequences:}

None.




\subsubsection{Create an order}

\textbf{Use Case:} Create an order\\
\textbf{Primary Actor:} Waiter\\
\textbf{Main Success Scenario:}

\begin{enumerate}
\item \label{item:w1_1} The waiter presses the ``Create a new order'' button.

\item The program shows a dialog box with input fields.

\item The waiter enters the number of the table into the ``Table'' field.

\item The administrator selects the menu items from the table the ones which will be the parts of the new order.

\item The waiter presses the OK button.

\item The program displays a message that the order was successfully created.

\end{enumerate}


\textbf{Alternative Sequences:}
\begin{enumerate}

\item The waiter presses the ``Cancel'' button.
\begin{itemize}
\item The dialog closes.
\item The scenario returns to step \ref{item:w1_1}.
\end{itemize}

\end{enumerate}




\subsubsection{Calculate the price an order}

\textbf{Use Case:} Calculate the price an order\\
\textbf{Primary Actor:} Waiter\\
\textbf{Main Success Scenario:}

\begin{enumerate}

\item \label{item:w2_1} The waiter selects in the table the order whose price will be calculated.

\item The waiter presses the ``Calculate price'' button.

\item The program displays a message with the price of the selected order.

\end{enumerate}

\textbf{Alternative Sequences:}
\begin{enumerate}

\item No order or more then one orders were selected before the button was pressed.
\begin{itemize}
\item The program displays an error message.
\item The scenario returns to step \ref{item:w2_1}.
\end{itemize}

\end{enumerate}





\subsubsection{Generate a bill}

\textbf{Use Case:} Generate a bill\\
\textbf{Primary Actor:} Waiter\\
\textbf{Main Success Scenario:}

\begin{enumerate}

\item \label{item:w3_1} The waiter selects in the table the order for which the bill will be created.

\item The waiter presses the ``Generate bill'' button.

\item The program displays a message that the bill was successfully created.

\end{enumerate}

\textbf{Alternative Sequences:}
\begin{enumerate}

\item No order or more then one orders were selected before the button was pressed.
\begin{itemize}
\item The program displays an error message.
\item The scenario returns to step \ref{item:w3_1}.
\end{itemize}

\end{enumerate}



\subsubsection{Receive an order}

\textbf{Use Case:} Receive an order\\
\textbf{Primary Actor:} Chef\\
\textbf{Main Success Scenario:}

\begin{enumerate}

\item  When a new order is placed the chef automatically receives the menu items of the order, decomposed into base products

\end{enumerate}

\textbf{Alternative Sequences:}

None.










\section{Design}
\subsection{Packages}
\label{sec:packages}
 \begin{figure*}[!htbp]
     \centering
     \includegraphics[width=0.8\linewidth]{figures/package.pdf}
     \caption{Package diagram} 
     \label{fig:package}
 \end{figure*}
The application follows a layered architecture: 
\begin{itemize}
    \item the \textbf{Data Access Layer} (\verb|data| package) contains the classes for serialization and deserialization, and for generating text files.
    \item the \textbf{Business Logic Layer} (\verb|business| package) contains the restaurant, order and product classes and interfaces. It also includes a validation package which was omitted in the final version.
    \item the \textbf{Presentation Layer} (\verb|presentation| package) contains the classes for external communication: the graphical user interface classes and their corresponding controllers.
\end{itemize}
Moreover there are some packages that are used in more than one layer:
\begin{itemize}
    \item \verb|observer| package contains a class and an interface for implementing the observer pattern.
    \item \verb|exception| package contains the exceptions that are thrown by the classes.
\end{itemize}
On figure \ref{fig:package} one can see the package diagram described above.



\subsection{Dependency Injection}
Similarly to the Order management project, dependency injection was used to interconnect the dependent classes, thus providing a low coupling between them. The main \verb|App| class solves these dependencies.






\section{Implementation}
\subsection{Observer}
\label{sec:observer}
\begin{figure*}[!htbp]
    \centering
    \includegraphics[width=0.5\linewidth]{figures/observer.pdf}
    \caption{The observer package} 
    \label{fig:observer}
\end{figure*}

The \verb|observer| package contains the \verb|Observable| class and the \verb|Observer| interface. These are inspired from the now deprecated class and interface of the \verb|java.util| package. With these, one can implement the observer pattern into any other class.

The \verb|Observer| is an object that wants to be triggered when a certain event occurs. The \verb|Observable| class can hold many of these objects, and when its method, the \verb|notifyObservers(T)| is called, all observers in the list are notified. Thus they can be used for one-to-many event signaling.

A difference from the implementation of the \verb|java.util| package is that the class and the interface are generic. Thus increasing the compile time type safety.





\subsection{Business logic layer}
\subsubsection{Menu items}
\label{sec:menu_item}

 \begin{figure*}[!htbp]
     \centering
     \includegraphics[width=0.8\linewidth]{figures/menu.pdf}
     \caption{The menu package} 
     \label{fig:menu}
 \end{figure*}

The menu items are the products that the clients can order. The \textbf{composite pattern} was used in order to allow creating more complex products, e.g. a menu made of a drink and a food.

The \verb|MenuItem| abstract class contains name of the product and the abstract methods, that are implemented in its children. 

Moreover, it overrides the \verb|int hashCode()| method, and sets the hash value of all the menu items equal to the hash of their name, which is immutable. Thus it's important to not allow external modification of this field. But the administrator must be able to change the name of any product. If we place the items in a \verb|HashMap|, having the hash code depending only on the name, if the name is changed, the data structure will arrive in an inconsistent state. A solution could be to make the products' name immutable and create a new object on each name change. But this would cause another error, more specifically, if the object is a component of a \verb|CompositeProduct|: the replacement of the object in the \verb|HashMap| will not be reflected on the \verb|CompositeProduct|s, i.e. they would point to different objects. To solve this, the \verb|void update(MenuItem)| method was created to update the current object with the given one. This way the address of the object would not change, thus not invalidating the compositions. Then after each update, the object is removed from the \verb|HashMap| and placed back again using the updated name.

The child classes specialize the \verb|MenuItem|. Both override the \verb|update| and \verb|equals| methods for the same purpose.

The \verb|BaseProduct| has a \verb|price| field which is directly accessed from the method \verb|double computePrice()|.

The \verb|CompositeProduct| contains a \verb|List| of \verb|MenuItem|s, thus allowing to have as components objects from its own type. This class also has a \verb|price| field. When it is set (not null), the \verb|computePrice| method returns its value. When it is not set (is null), the method returns the sum of the computed prices of the components. This mechanism was implemented to provide setting an explicit price on a composite product. I.e. for setting a lower price for a hamburger menu than the sum of the prices of each food and drink in it.

Two enums were created to ease the operations of the controllers: 
\begin{itemize}
 \item \verb|ProductType|: Type of the product. Can be either base or composed one.
 
 \item \verb|ProductPriceType|: Used for selecting the correct operations when composed items are created or updated by the GUI.
 \begin{itemize}
  \item \verb|EXPLICIT|: the \verb|price| field will be used for computing the price;
  \item \verb|AUTOMATIC|: the price computed by calling the \verb|computePrice| method of the components.
  
 \end{itemize}

\end{itemize}




\subsubsection{Order}
\label{sec:order}

 \begin{figure*}[!htbp]
     \centering
     \includegraphics[width=0.3\linewidth]{figures/order.pdf}
     \caption{The Order class} 
     \label{fig:order}
 \end{figure*}

\verb|Order| is an immutable class representing an order placed by a waiter. Being immutable and having its \verb|hashcode| and \verb|equals| methods overridden, it's great for being the key of a \verb|HashMap|.




\subsubsection{Restaurant}
\label{sec:restaurant}

 \begin{figure*}[!htbp]
     \centering
     \includegraphics[width=0.6\linewidth]{figures/restaurant.pdf}
     \caption{The Restaurant class} 
     \label{fig:restaurant}
 \end{figure*}

The \verb|IRestaurant| interface provides the main functions the restaurant has. All three types of employees use this interface to work with menu items and orders.The \verb|Restaurant| class implements this interface. T

\subsubsubsection{Menu items of the restaurant}
For the \verb|MenuItem|, all the CRUD operations are implemented. Sometimes the target item is specified by its name or by the object itself. E.g.:
\begin{verbatim}
  void updateMenuItem(String itemName, MenuItem newItem);
  void updateMenuItem(MenuItem menuItem);
\end{verbatim}

In the \verb|Restaurant| implementation, the \verb|MenuItem| objects are stored in a \verb|HashMap|, having as key their names. Thus the problem specified in (\ref{sec:menu_item}) occurred, and the explained solution was applied. Consequently, one uses the \verb|updateMenuItem| method with single argument, when  the name of the object was not changed, and uses the method with two arguments in the other case.

\subsubsubsection{Orders of the restaurant}
The create, get and contains operations were implemented for the orders. Moreover, the calculation of the total price of the order, and the generation of bills was also provided. The \verb|Restaurant| class also allows other objects to subscribe to the event of new order creation, by using the \verb|Observer| class described in (\ref{sec:observer}).

\subsubsubsection{Design by contract}
The design by contract principle was applied for the restaurant class and interface. This was represented  by using the \verb|@pre|, \verb|@post|, \verb|@result|, \verb|@nochange|, \verb|@forall| and \verb|@result| (custom) JavaDoc tags. These conditions are checked by placing assertions for the pre-conditions at the beginning, and assertions for the post-conditions at the end of each method.

The class invariant is represented by the \verb|protected boolean isWellFormed()| method, and the \verb|@invariant| tag. This method is asserted at the beginning and the end of each method in order to verify the integrity of the state.


\subsubsection{Validation}
This package was not used in the final version because the usage of assertions and design by contract. The \verb|Validator|s provide a mechanism, where one can chain together multiple validators of the generic same type, and evaluate objects by all of them. This was inspired by the \verb|java.util.function.Consumer| interface.

\subsection{Presentation layer}

 \begin{figure*}[!htbp]
     \centering
     \includegraphics[width=0.6\linewidth]{figures/presentation.pdf}
     \caption{The most important classes of the presentation package} 
     \label{fig:presentation}
 \end{figure*}

The \verb|presentation| package contains the necessary classes for creating the three graphical user interfaces, and for providing the interconnection with the business layer.

\subsubsection{View}
\label{sec:view}
For each three graphical user interfaces, in their own packages, separate \verb|JFrame| classes were created, and for each input dialog box (\verb|EditItemPanel|, \verb|OrderInputPanel|) a separate \verb|JPanel|. \cite{geeks:2}

\subsubsection{Controller}
\label{sec:controller}
The controllers connect the GUI to the business layer. They also provide the logic behind the GUI.

The chef does not have a controller, being only a passive observer. For the other two types of GUIs, the controller subscribes to button presses, modifies the  GUI through its methods, receives the values of the input forms from the GUI, then sends and receives data to/from an IRestaurant object. 

Because of the complexity of the menu item editor panel, a separate controller was created for it.

\subsection{Data layer}
The \verb|data| package provides the communication between the files and the business layer.

\subsubsection{Serializator}
\label{sec:serialize}

 \begin{figure*}[!htbp]
     \centering
     \includegraphics[width=0.3\linewidth]{figures/ser.pdf}
     \caption{The Serializator class} 
     \label{fig:ser}
 \end{figure*}


A generic \verb|Serializator| \cite{geeks:1} was implemented to serialize and deserialize objects to/from files.

\subsubsection{Bill generator}
\label{sec:bill}

\begin{figure*}[!htbp]
     \centering
     \includegraphics[width=0.5\linewidth]{figures/bill.pdf}
     \caption{The BillGenerator interface and implementation} 
     \label{fig:bill}
 \end{figure*}

The \verb|FileBillGenerator| implementation of the \verb|BillGenerator| creates a text file for a given order, and its corresponding menu items and total sum.

\section{Result}
The application was verified by testing the edge case user inputs of the GUI.

Because the usage of dependency injection and interfaces for constructor parameter, dependency was provided on abstraction instead of concretion. This eases the unit testing, because mock objects could be given to the constructors of the dependent classes.

\section{Conclusion}
The main objective was achieved by completing the secondary objectives.

Graphical user interfaces were created for each types of users, and their communication with the business logic was realized through the controllers.

Bills were created into text files and the whole state of the restaurant was saved into a single file by using serialization. On the startup of the program from this file the class could be successfully loaded back.

The design patterns were essential to achieve the main objective:
\begin{itemize}
 \item Dependency injection for reducing coupling between classes.
 \item Observer pattern to notify the chef GUI of a new order.
 \item Composite pattern to represent more complex types of product.
\end{itemize}


The application can be further developed by:
\begin{itemize}
 \item giving to the chef more operations to be done. I.e. mark item done after its preparation was finished.
 \item improving the layout of the GUI.
\end{itemize}

\bibliography{bibliography}
\bibliographystyle{plain}

\end{document}
